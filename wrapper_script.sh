#!/bin/bash

# turn on bash's job control
# set -m

# Start the primary process and put it in the background
./start_javalin.sh -D
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start my_first_process: $status"
  exit $status
fi
# Send a pdf which will force the failure
./force_fail.sh

while sleep 60; do
  ps aux |grep my_first_process |grep -q -v grep
  PROCESS_1_STATUS=$?

  if [ $PROCESS_1_STATUS -ne 0 ]; then
    echo "process has  exited."
    exit 1
  fi
done
