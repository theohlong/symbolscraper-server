/******************************************************************************
* BoundingBoxExtractor.java
*
* Copyright (c) 2018, 2019
* Ritvik Joshi, Parag Mali, Puneeth Kukkadapu, Mahshad Mahdavi, and 
* Richard Zanibbi
*
* Document and Pattern Recognition Laboratory
* Rochester Institute of Technology, USA
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
******************************************************************************/

package org.dprl.symbolscraper.TrueBox;

import org.apache.pdfbox.contentstream.operator.color.*;
import org.apache.pdfbox.pdmodel.PDDocument;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.color.PDColor;
import org.apache.pdfbox.pdmodel.graphics.state.PDGraphicsState;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;
import org.dprl.symbolscraper.TrueBox.Domain.*;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

class CharacterExtractor extends PDFTextStripper {
    //class BoundingBox extends DrawPrintTextLocations {
    int pageNum;
    PageStructure currentPage;
    float prevBaseLineY=0;
    float prevBaseLineX=0;
    public int lineId=0;
    public int wordId=0;
    public int charId=0;
    int charGlyphId=0;
    float lineStartX=0;
    float lineStartY=0;
    ArrayList<TextColor> colorList = new ArrayList<>();
    ArrayList<Word> wordList = new ArrayList<>();
    ArrayList<PathWrapper> charList = new ArrayList<>();

    public CharacterExtractor(PDDocument doc, int pageNum, PageStructure page) throws IOException {
        super();
        document = doc;
        this.pageNum = pageNum;
        this.currentPage = page;
        setOperators();
    }
    
    public void setOperators(){
        addOperator(new SetStrokingColorSpace());
        addOperator(new SetNonStrokingColorSpace());
        addOperator(new SetStrokingDeviceCMYKColor());
        addOperator(new SetNonStrokingDeviceCMYKColor());
        addOperator(new SetNonStrokingDeviceRGBColor());
        addOperator(new SetStrokingDeviceRGBColor());
        addOperator(new SetNonStrokingDeviceGrayColor());
        addOperator(new SetStrokingDeviceGrayColor());
        addOperator(new SetStrokingColor());
        addOperator(new SetStrokingColorN());
        addOperator(new SetNonStrokingColor());
        addOperator(new SetNonStrokingColorN());
    }

    public void extractCharacters() throws IOException {
        this.setStartPage(pageNum + 1);
        this.setEndPage(pageNum + 1);
        if (pageNum==25) {
            System.out.print("k");
        }
        Writer dummy = new OutputStreamWriter(new ByteArrayOutputStream());
        writeText(document, dummy);
        currentPage = combineIntersectingGCInPageStructure(currentPage);
		// RZ: Modified to get the current rather than first page (0) for BB
        PDPage page = document.getPage(currentPage.pageId);

        BBOX pdfBoundingBox = new BBOX(page.getBBox().getLowerLeftX(), page.getBBox().getLowerLeftY(), page.getBBox().getWidth(), page.getBBox().getHeight());
        currentPage.meta = new MetaData(lineId, wordId, charId, pdfBoundingBox);
        if (currentPage.lines.size() == 0) return;
        Line prevLine = currentPage.lines.get(currentPage.lines.size()-1);
        prevLine.baseLine = new BaseLine(lineStartX,lineStartY,prevBaseLineX,prevBaseLineY);

        dummy.close();
    }

    @Override
    protected void writeString(String string, List<TextPosition> textPositions) {


        byte[] b = string.getBytes();
        // capture the nop char code, null character
        if (b.length < 1 || b[0] == 0 || b[0] == 1) {
            return;
        }

        boolean singleWord=false;
        String currentword="";
        String[] word = string.split(getWordSeparator());

        if(word.length==1){
            singleWord=true;
        }

        TextPosition textLine = textPositions.get(0);

        // if the baseline has changed start a new line
        if(textLine.getTextMatrix().getTranslateY()!= prevBaseLineY){
            // if the previous line exist set its baseline values
            if (prevBaseLineY != 0) {
                Line prevLine = currentPage.lines.get(currentPage.lines.size() - 1);
                prevLine.baseLine = new BaseLine(lineStartX, lineStartY, prevBaseLineX, prevBaseLineY);
            }
            wordList =  new ArrayList<>();
            currentPage.lines.add(new Line(lineId,null, wordList));

            lineId+=1;
            prevBaseLineY = textLine.getTextMatrix().getTranslateY();
            lineStartY = textLine.getTextMatrix().getTranslateY();
            lineStartX = textLine.getTextMatrix().getTranslateX();
        }

        for (TextPosition text : textPositions) {
            if (text.getUnicode().equals(getWordSeparator()) || text.getUnicode().equals("\uF020")) {
                wordList.add(new Word(wordId, charList, currentword));
                wordId++;
                charList = new ArrayList<>();
                currentword = "";
                continue;
            }

            String value = text.getUnicode();

            TextColor textColor = null;
            if (colorList.get(charGlyphId).unicode.equals(value)) {
                textColor = colorList.get(charGlyphId);
            }
            // If new characters are needed please search through the getters within getGraphicsState()
            // getGraphicsState().getTextState() will be helpful as well as getGraphicsState().getTextState().getFont()


            String fontName;
            float fontWeight;
            float italicAngle;
            try {
                fontName = text.getFont().getFontDescriptor().getFontName();
                fontWeight = text.getFont().getFontDescriptor().getFontWeight();
                italicAngle = text.getFont().getFontDescriptor().getItalicAngle();
            } catch (Exception e) {
                fontName = "Unable to derive font name";
                fontWeight = 0;
                italicAngle = 0;
            }

            // float uhh = text.getFontSize();
            float fontSize = text.getFontSizeInPt();

            CharGlyph charGlyph = new CharGlyph(
                    charGlyphId,
                    charId,
                    value,
                    text,
                    null,
                    null,
                    wordId,
                    lineId - 1,
                    textColor,
                    fontName,
                    fontSize,
                    fontWeight,
                    italicAngle);


            charList.add(new Char(charGlyph, charGlyph.value, charId));
            charId++;
            charGlyphId++;
            currentword += value;
            prevBaseLineX = text.getTextMatrix().getTranslateX() + text.getWidthDirAdj();
        }
        if(singleWord){
            wordList.add(new Word(wordId, charList,word[0]));
            wordId++;
            charList= new ArrayList<>();
        }
    }

	@Override
    protected void processTextPosition(TextPosition text)
    {
        super.processTextPosition(text);

        PDColor nonStrokingColor = getGraphicsState().getNonStrokingColor();
        String unicode = text.getUnicode();

        colorList.add(new TextColor(nonStrokingColor,unicode));
    }

    public PageStructure combineIntersectingGCInPageStructure(PageStructure pageStructure)  {
        pageStructure.lines = pageStructure.lines.stream()
                .map(this::combineIntersectingGCinLine)
                .collect(Collectors.toCollection(ArrayList::new));

        pageStructure.update();
        return  pageStructure;
    }

    public Line combineIntersectingGCinLine(Line line) {
        line.setWords(
                line.getWords().stream()
                        .map(this::combineIntersectingGCinWord)
                        .collect(Collectors.toCollection(ArrayList::new)));
        line.update();
        return  line;
    }

    public Word combineIntersectingGCinWord(Word word) {


        if (word.getConstituents().size() < 1) {
            word.setBoundingBox(new BBOX(prevBaseLineX, prevBaseLineY, 0f,0f));
        }

        word.setConstituents(
                word.getConstituents().stream()
                        .map(this::combineIntersectionGCinChar)
                        .collect(Collectors.toCollection(ArrayList::new)));

        if (word.getConstituents().size() < 1) {
            word.update();
            return word;
        }

        /*
        Char prevChar = (Char) word.getConstituents().remove(0);
        prevChar = (Char) prevChar.clone();
        ArrayList<PathWrapper> finalChars = new ArrayList<>();
        for (PathWrapper wordEntry: word.getConstituents()) {
            Char curChar = (Char) wordEntry;
            if (prevChar.overlap(curChar)) {
                if (prevChar.intersect(curChar)) {
                    prevChar.consume(curChar, true);
                } else {
                    finalChars.add((Char) prevChar.clone());
                    prevChar = curChar;
                }
            } else {
                // add to final char glyphs
                finalChars.add((Char) prevChar.clone());
                prevChar = curChar;
            }
        }
        finalChars.add((Char) prevChar.clone());
        word.setConstituents(finalChars);
        */

        word.update();
        return word;
    }

    public Char combineIntersectionGCinChar(PathWrapper wordEntry) {
        // at this point each char has exactly one charGlyph
        // in it as Graphics have not been added and nothing
        // has been combined up to this point
        Char c = (Char) wordEntry;
        Char nChar = new Char(FontUtils.drawGlyphAndExtractBBox(c.getCharConstituents().get(0)),c.value, c.charId);
        nChar.updateBoundingBox(nChar);
        return nChar;
    }
}

