package org.dprl.symbolscraper.TrueBox.PostProcessor;

import org.dprl.symbolscraper.TrueBox.Domain.*;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;

import java.util.ArrayList;
import java.util.function.Function;

public class MathPostProcessor implements Function<PageStructure, PageStructure> {
    @Override
    public PageStructure apply(PageStructure pageStructure) {
        for (Line l: pageStructure.lines) {
            for (Word w: l.getWords()) {
                ArrayList<PathWrapper> newWordEntries = new ArrayList<>();
                for(PathWrapper we: w.getConstituents()) {
                    if (we instanceof Char) newWordEntries.add(convertSquareRoots((Char)we));
                    else newWordEntries.add(we);
                }
                w.setConstituents(newWordEntries);
            }
        }
        return pageStructure;
    }

    private Char convertSquareRoots(Char c) {
        boolean isRadical = false;
        boolean isLine = false;
        for (CharGlyph charGlyph: c.getCharConstituents()) {
            if (charGlyph.value.equals('\u221A') || charGlyph.value.equals('\u23B7') || charGlyph.value.equals("√")) {
                isRadical = true;
                break;
            }
        }
        for (GraphicGlyph graphicsItem: c.getGraphicConstituents()) {
            if (graphicsItem.descriptor.equals(GraphicGlyph.graphicsLine)) {
                isLine = true;
                break;
            }
        }

        if (isRadical && isLine) {
            c.value = "Square Root (√)";
        }

        return c;
    }
}
