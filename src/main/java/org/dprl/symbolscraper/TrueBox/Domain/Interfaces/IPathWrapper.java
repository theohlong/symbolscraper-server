package org.dprl.symbolscraper.TrueBox.Domain.Interfaces;


import org.dprl.symbolscraper.TrueBox.Domain.CharGlyph;
import org.dprl.symbolscraper.TrueBox.Domain.GraphicGlyph;

import java.util.ArrayList;

public interface IPathWrapper<A> {

    boolean intersect(A otherItem);

    boolean intersect(GeneralPathType otherItem);

    ArrayList<CharGlyph> getCharConstituents();

    ArrayList<GraphicGlyph> getGraphicConstituents();
}
