package org.dprl.symbolscraper.TrueBox.Domain;

import org.apache.pdfbox.text.TextPosition;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.GeneralPathType;


import java.awt.geom.GeneralPath;
import java.util.Arrays;

public class CharGlyph extends GeneralPathType {

    public int id;
    public int charId;
    public String value;
    public int wordID;
    public int lineID;

    public String fontName;
    public float fontSize;
    public float fontWeight;
    public float italicAngle;

    public org.apache.pdfbox.text.TextPosition charInfo;
    TextColor textcolor;

    public CharGlyph(int id,
                     int charId,
                     String value,
                     TextPosition text,
                     BBOX bbox,
                     GeneralPath glyph,
                     int wordID,
                     int lineID,
                     TextColor textcolor,
                     String fontName,
                     float fontSize,
                     float fontWeight,
                     float italicAngle
    ){
        this.id=id;
        this.charId=charId;
        this.value=value;
        this.charInfo=text;
        this.boundingBox=bbox;
        this.wordID=wordID;
        this.lineID=lineID;
        this.generalPath=glyph;
        this.textcolor = textcolor;
        this.fontName = fontName;
        this.fontSize = fontSize;
        this.fontWeight = fontWeight;
        this.italicAngle = italicAngle;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            CharGlyph cg = new CharGlyph(
                    this.id,
                    this.charId,
                    this.value,
                    this.charInfo,
                    (BBOX) this.boundingBox.clone(),
                    (GeneralPath) this.generalPath.clone(),
                    this.wordID,
                    this.lineID,
                    (TextColor) this.textcolor.clone(),
                    fontName,
                    fontSize,
                    fontWeight,
                    italicAngle);
            cg.boundingBox = boundingBox;
            return cg;
        }
    }

    @Override
    public Element asXml(Config c) {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("CharGlyph");
        if(c.fields.writeFields.writeId)
            root.addAttribute("id", this.id+"");

        if (c.fields.writeFields.writeCharGlyphBBOX)
            root.addAttribute("BBOX", boundingBox.startX + " " +
                                        +boundingBox.startY + " " +
                                        +boundingBox.width + " " +
                                        +boundingBox.height);
        if(c.fields.writeFields.charAttributes.writeColor)
            root.addAttribute("RGB", colorString());

        if(c.fields.writeFields.charAttributes.writeFontName)
            root.addAttribute("fontName", fontName);

        if(c.fields.writeFields.charAttributes.writeFontSize)
            root.addAttribute("fontSize", fontSize+"");

        if(c.fields.writeFields.charAttributes.writeBold)
            root.addAttribute("fontWeight", fontWeight+"");

        if(c.fields.writeFields.charAttributes.writeItalicAngle)
            root.addAttribute("fontSize", italicAngle+"");

        root.addText(value);
        return root;
    }

    public String colorString() {

        TextColor textColor = this.textcolor;
        String color;
        if (textColor == null) {
            color = "unknown";
        } else {
            color = Arrays.toString(textColor.color.getComponents());
        }
        return color;
    }
}