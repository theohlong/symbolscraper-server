package org.dprl.symbolscraper.TrueBox.Domain;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.Boundable;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;

import java.util.ArrayList;

public class Word extends Boundable{
    int wordId;
    String wordString;
    protected ArrayList<PathWrapper> constituents;

    public Word(int wordId, ArrayList<PathWrapper> constituents, String wordString){
        this.wordId=wordId;
        this.constituents =constituents;
        this.wordString=wordString;
    }

    @Override
    protected Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            Word w = new Word(
                    this.wordId,
                    (ArrayList<PathWrapper>)this.constituents.clone(),
                    this.wordString);
            w.boundingBox = boundingBox;
            return w;
        }
    }


    public Element asXml(Config c) {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("Word");

        if(c.fields.writeFields.writeWordsBBOX) {
            System.out.print("hii");
            root.addAttribute("BBOX", this.boundingBox.startX + " " +
                    this.boundingBox.startY + " " +
                    this.boundingBox.width + " " +
                    this.boundingBox.height);
        }
        if(c.fields.writeFields.writeId)
            root.addAttribute("id",wordId+"");
        return root;
    }

    public ArrayList<PathWrapper> getConstituents() {
        return constituents;
    }

    public void setConstituents(ArrayList<PathWrapper> constituents) {
        this.constituents = constituents;
    }

    @Override
    public void consume(Boundable boundable, boolean update) {
        if (boundable instanceof Char) {
            Char c = (Char) boundable;
            constituents.add(c);
        } else if (boundable instanceof Graphic) {
            Graphic g = (Graphic) boundable;
            constituents.add(g);
        } else {
            // TODO add exceptions
        }
        if (update) update();
    }

    @Override
    public void update() {
        constituents.forEach(this::updateBoundingBox);
    }
}
