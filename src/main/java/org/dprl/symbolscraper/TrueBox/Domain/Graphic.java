package org.dprl.symbolscraper.TrueBox.Domain;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.Boundable;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;

import java.awt.geom.Point2D;
import java.util.ArrayList;

public class Graphic extends PathWrapper {

    public String descriptor;
    public int graphicId;

    private Graphic(ArrayList<GraphicGlyph> gGlyphs, String descriptor, int graphicId) {
        this.descriptor = descriptor;
        this.charConstituents = new ArrayList<>();
        this.graphicConstituents = gGlyphs;
        this.graphicId = graphicId;
    }

    public Graphic(GraphicGlyph initGraphicGlyph, String descriptor, int graphicId) {
        graphicConstituents = new ArrayList<>();
        graphicConstituents.add(initGraphicGlyph);
        charConstituents = new ArrayList<>();

        updateBoundingBox(initGraphicGlyph);
        this.descriptor = descriptor;
        this.graphicId = graphicId;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            Graphic g = new Graphic(graphicConstituents, descriptor, graphicId);
            g.setBoundingBox(boundingBox);
            g.charConstituents = charConstituents;
            return g;
        }
    }

    @Override
    public Element asXml(Config c) {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("Graphic");
        if (graphicConstituents.size() == 1) {
            GraphicGlyph singleGraphicsItem = graphicConstituents.get(0);
            root.addText(singleGraphicsItem.descriptor);
            if(c.fields.writeFields.writePoints) {
                StringBuilder ptsStr = new StringBuilder();
                for (Point2D pt: singleGraphicsItem.points) {
                    ptsStr.append("p ").append(pt.getX()).append(" ").append(pt.getY()).append(" ");
                }
                root.addAttribute("points", ptsStr.toString());
            }
        } else if (graphicConstituents.size() > 1) {
            if (c.fields.writeFields.writeId) root.addAttribute("id", this.graphicId + "");
            root.addAttribute("descriptor", descriptor);
            for (GraphicGlyph gi : graphicConstituents) {
                root.add(gi.asXml(c));
            }
        }
        if (c.fields.writeFields.writeGraphicBBOX)
            root.addAttribute("BBOX", this.boundingBox.startX + " " +
                    +this.boundingBox.startY + " " +
                    +this.boundingBox.width + " " +
                    +this.boundingBox.height);

        if (c.fields.writeFields.writeId)
            root.addAttribute("id", this.graphicId+"");
        return root;
    }

    @Override
    public void consume(Boundable boundable, boolean update) {
        if (boundable instanceof Char) {
            Char c = (Char) boundable;
            this.descriptor = this.descriptor + c.value;
            charConstituents.addAll(c.getCharConstituents());
            graphicConstituents.addAll(c.getGraphicConstituents());
        } else if (boundable instanceof CharGlyph) {
            CharGlyph cg = (CharGlyph) boundable;
            this.descriptor = this.descriptor + cg.value;
            charConstituents.add(cg);
        } else if (boundable instanceof GraphicGlyph) {
            GraphicGlyph gg = (GraphicGlyph) boundable;
            this.descriptor = this.descriptor + gg.descriptor;
            graphicConstituents.add(gg);
        } else if (boundable instanceof Graphic) {
            Graphic g = (Graphic) boundable;
            this.descriptor = this.descriptor + g.descriptor;
            charConstituents.addAll(g.getCharConstituents());
            graphicConstituents.addAll(g.getGraphicConstituents());
        }
        if (update) update();
    }
}
