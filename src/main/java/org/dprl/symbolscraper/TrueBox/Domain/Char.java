package org.dprl.symbolscraper.TrueBox.Domain;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.Boundable;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;

import java.util.ArrayList;

public class Char extends PathWrapper {

    public String value;
    public int charId;

    private Char(ArrayList<CharGlyph> cGlyphs, String value, int charId) {
        this.value = value;
        this.charConstituents = cGlyphs;
        this.graphicConstituents = new ArrayList<>();
        this.charId = charId;
    }

    public Char(CharGlyph initCharGlyph, String value, int charId) {
        charConstituents = new ArrayList<>();
        charConstituents.add(initCharGlyph);
        graphicConstituents = new ArrayList<>();

        updateBoundingBox(initCharGlyph);
        this.value = value;
        this.charId = charId;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            Char c = new Char(charConstituents, value, charId);
            c.boundingBox = boundingBox;
            c.graphicConstituents = graphicConstituents;
            return c;
        }
    }

    @Override
    public Element asXml(Config c) {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("Char");
        if (charConstituents.size() > 0 && graphicConstituents.size() > 0) {
            root.addAttribute("value", value);
            for (CharGlyph cg : charConstituents) {
                root.add(cg.asXml(c));
            }
            for (GraphicGlyph gi : graphicConstituents) {
                root.add(gi.asXml(c));
            }
        } else {
            if (charConstituents.size() == 1) {
                CharGlyph singleGlyph = charConstituents.get(0);

                if (c.fields.writeFields.charAttributes.writeColor)
                    root.addAttribute("RGB", singleGlyph.colorString());

                if(c.fields.writeFields.charAttributes.writeFontName)
                    root.addAttribute("fontName", singleGlyph.fontName);

                if(c.fields.writeFields.charAttributes.writeFontSize)
                    root.addAttribute("fontSize", singleGlyph.fontSize+"");

                if(c.fields.writeFields.charAttributes.writeBold)
                    root.addAttribute("fontWeight", singleGlyph.fontWeight+"");

                if(c.fields.writeFields.charAttributes.writeItalicAngle)
                    root.addAttribute("italicAngle", singleGlyph.italicAngle+"");

                root.addText(singleGlyph.value);

            } else {
                root.addAttribute("value", value);
                for (CharGlyph cg : charConstituents) {
                    root.add(cg.asXml(c));
                }
            }
            if (graphicConstituents.size() == 1) {
                GraphicGlyph singleGraphicsItem = graphicConstituents.get(0);
                root.addText(singleGraphicsItem.descriptor);
            } else if (graphicConstituents.size() > 1) {

                root.addAttribute("value", value);
                for (GraphicGlyph gi : graphicConstituents) {
                    root.add(gi.asXml(c));
                }
            }
        }
        if (c.fields.writeFields.writeId) root.addAttribute("id", this.charId + "");
        if (c.fields.writeFields.writeCharBBOX)
                root.addAttribute("BBOX", this.boundingBox.startX + " " +
                                        +this.boundingBox.startY + " " +
                                        +this.boundingBox.width + " " +
                                        +this.boundingBox.height);
        return root;
    }

    @Override
    public void consume(Boundable boundable, boolean update) {

        if (boundable instanceof Char) {
            Char c = (Char) boundable;
            this.value = this.value + c.value;
            charConstituents.addAll(c.charConstituents);
            graphicConstituents.addAll(c.graphicConstituents);
        } else if (boundable instanceof CharGlyph) {
            CharGlyph cg = (CharGlyph) boundable;
            this.value = this.value + cg.value;
            charConstituents.add(cg);
        } else if (boundable instanceof GraphicGlyph) {
            GraphicGlyph gg = (GraphicGlyph) boundable;
            this.value = this.value + gg.descriptor;
            graphicConstituents.add(gg);
        } else if (boundable instanceof Graphic) {
            Graphic g = (Graphic) boundable;
            this.value = this.value + g.descriptor;
            charConstituents.addAll(g.getCharConstituents());
            graphicConstituents.addAll(g.getGraphicConstituents());
        }
        if (update) update();
    }
}
