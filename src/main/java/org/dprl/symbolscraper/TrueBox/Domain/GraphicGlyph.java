package org.dprl.symbolscraper.TrueBox.Domain;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.GeneralPathType;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.IXMLWriteable;

import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class GraphicGlyph extends GeneralPathType implements IXMLWriteable {

    public static final String graphicsLine = "-line-";
    public int id;
    public final String descriptor;
    public ArrayList<Point2D> points;

    public final Float angle;

    public GraphicGlyph(int id, BBOX boundingBox, GeneralPath glyph, String descriptor, ArrayList<Point2D> points){
        this.id = id;
        this.boundingBox=boundingBox;
        this.generalPath = glyph;
        this.descriptor = descriptor;
        this.points = points;
        if (this.points.size() == 2) {
            Point2D p1 = this.points.get(1);
            Point2D p2 = this.points.get(1);
            angle = (float) Math.atan2(p2.getY() - p1.getY(), p2.getX() - p1.getX());
        } else {
            angle = null;
        }
    }

    @Override
    protected Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return new GraphicGlyph(id, (BBOX) boundingBox.clone(), generalPath, descriptor, points);
        }
    }

    @Override
    public Element asXml(Config c) {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("GraphicsItem");

        if(c.fields.writeFields.writeGraphicBBOX)
            root.addAttribute("BBOX", boundingBox.startX + " " +
                    boundingBox.startY + " " +
                    boundingBox.width + " " +
                    boundingBox.height);

        if(c.fields.writeFields.writeId)
            root.addAttribute("id", this.id + "");

        if(c.fields.writeFields.writePoints) {
            StringBuilder ptsStr = new StringBuilder();
            for (Point2D pt: this.points) {
                ptsStr.append("p ").append(pt.getX()).append(" ").append(pt.getY()).append(" ");
            }
            root.addAttribute("points", ptsStr.toString());
        }
        root.addText(descriptor);

        return root;
    }
}