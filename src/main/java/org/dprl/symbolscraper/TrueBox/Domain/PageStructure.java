/******************************************************************************
* PageStructure.java
*
* Copyright (c) 2018, 2019
* Ritvik Joshi, Parag Mali, Puneeth Kukkadapu, Mahshad Mahdavi, and 
* Richard Zanibbi
*
* Document and Pattern Recognition Laboratory
* Rochester Institute of Technology, USA
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
******************************************************************************/

package org.dprl.symbolscraper.TrueBox.Domain;
import java.util.ArrayList;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.Boundable;

public class PageStructure extends Boundable implements Cloneable {
    public int pageId;
    public ArrayList<Line> lines;
    public ArrayList<Graphic> graphics;
    public MetaData meta;

    public PageStructure(int pgId, ArrayList<Line> sent, ArrayList<Graphic> graphics, MetaData meta){
        this.pageId=pgId;
        this.lines =sent;
        this.graphics = graphics;
        this.meta =meta;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            PageStructure ps = new PageStructure(
                    this.pageId,
                    (ArrayList<Line>) this.lines.clone(),
                    (ArrayList<Graphic>) this.graphics.clone(),
                    (MetaData) this.meta.clone()
            );
            return ps;
        }
    }

    public Element asXml(Config c) {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("Page");

        if(c.fields.writeFields.writeId)
            root.addAttribute("id", this.pageId+"");

        if (c.fields.writeFields.writePageBorderBBOX)
            root.addAttribute("BBOX", 
					meta.pdfBoundingBox.startX + " " +
                            meta.pdfBoundingBox.startY + " " +
                            meta.pdfBoundingBox.width + " " +
                            meta.pdfBoundingBox.height);


 		// RZ: Giving content bbox a different attribute name.
        if (c.fields.writeFields.writeContentBBOX)
            root.addAttribute("CBOX", 
					this.boundingBox.startX + " " +
                            this.boundingBox.startY + " " +
                            this.boundingBox.width + " " +
                            this.boundingBox.height);


        return root;
    }

    @Override
    public void consume(Boundable boundable, boolean update) {
        if (boundable instanceof Line) {

        } else {
            // TODO add exceptions
        }
    }

    @Override
    public void update() {
        if (lines.size() >0 ) lines.forEach(this::updateBoundingBox);
        else this.setBoundingBox(new BBOX(0,0,0,0));
    }
}









