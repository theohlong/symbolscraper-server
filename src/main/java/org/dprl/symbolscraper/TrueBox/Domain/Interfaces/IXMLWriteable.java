package org.dprl.symbolscraper.TrueBox.Domain.Interfaces;

import org.dom4j.Element;
import org.dprl.config.Config;

public interface IXMLWriteable {
    Element asXml(Config c);
}
