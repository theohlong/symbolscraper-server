package org.dprl.symbolscraper.TrueBox.Domain;
import org.apache.pdfbox.pdmodel.graphics.color.PDColor;

public class TextColor implements Cloneable{
    public PDColor color;
    public String unicode;

    public TextColor(PDColor color, String unicode){
        this.color=color;
        this.unicode=unicode;
    }

    @Override
    protected Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return new TextColor(this.color, this.unicode);
        }
    }
}