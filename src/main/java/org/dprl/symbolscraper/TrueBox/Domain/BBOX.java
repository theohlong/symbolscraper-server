package org.dprl.symbolscraper.TrueBox.Domain;

public class BBOX implements Cloneable{
    public float startX;
    public float startY;
    public float width;
    public float height;

    public BBOX(float x,float y, float w,float h){
        this.startX=x;
        this.startY=y;
        this.width=w;
        this.height=h;
    }

    @Override
    protected Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return new BBOX(
                    this.startX,
                    this.startY,
                    this.width,
                    this.height);
        }
    }

    public BBOX updateBoundingBox(BBOX bb) {
        if (bb == null) return (BBOX) this.clone();
        float newStartX = Math.min(this.startX, bb.startX);
        float newStartY = Math.min(this.startY, bb.startY);
        float newWidth = Math.max(
                this.startX + this.width,
                bb.startX + bb.width
        );
        float newHeight = Math.max(
                this.startY + this.height,
                bb.startY + bb.height
        );
        return new BBOX(newStartX, newStartY, newWidth - newStartX, newHeight - newStartY);
    }

    public boolean overlap(BBOX bb){

        if (bb.startX > (this.startX+this.width) || this.startX > (bb.startX+bb.width))
            return false;
        return !((bb.startY + bb.height) < (this.startY)) && !((this.startY + this.height) < (bb.startY));
    }

    public boolean contain(BBOX otherBox) {
        return this.startY <= otherBox.startY &&
                this.startX <= otherBox.startX &&
                (this.height + this.startY) >= (otherBox.height + otherBox.startY) &&
                (this.width + this.startX) >= (otherBox.width + otherBox.startX);
    }

    public float intersectionOverUnion(BBOX otherBox) {
        float startX = Math.max(otherBox.startX, this.startX);
        float startY = Math.max(otherBox.startY, this.startY);

        float endX = Math.min((otherBox.startX + otherBox.width), (this.startX + this.width));
        float endY = Math.max((otherBox.startY + otherBox.height), (otherBox.startX + otherBox.width));

        float width = endX - startX;
        float height = endY - startY;

        if (width < 0) return 0f;
        if (height < 0) return 0f;

        float intersection = (width * height);
        float union = (this.height * this.width) + (otherBox.height * otherBox.width);
        if (union <= 0) return 0f;
        return intersection/union;
    }


}
