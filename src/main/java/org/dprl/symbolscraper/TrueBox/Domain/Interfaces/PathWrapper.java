package org.dprl.symbolscraper.TrueBox.Domain.Interfaces;

import org.dprl.symbolscraper.TrueBox.Domain.CharGlyph;
import org.dprl.symbolscraper.TrueBox.Domain.GraphicGlyph;

import java.util.ArrayList;

public abstract class PathWrapper extends Boundable implements IPathWrapper<PathWrapper> {
    protected ArrayList<CharGlyph> charConstituents;
    protected ArrayList<GraphicGlyph> graphicConstituents;

    public boolean intersect(PathWrapper otherItem) {
        for (GeneralPathType generalPathType: otherItem.charConstituents) {
            if (this.intersect(generalPathType)) return true;
        }
        for (GeneralPathType generalPathType: otherItem.graphicConstituents) {
            if (this.intersect(generalPathType)) return true;
        }

        return false;
    }

    public boolean intersect(GeneralPathType otherItem) {
        for (GeneralPathType generalPathType: charConstituents) {
            if (generalPathType.intersect(otherItem)) return true;
        }
        for (GeneralPathType generalPathType: graphicConstituents) {
            if (generalPathType.intersect(otherItem)) return true;
        }

        return false;
    }

    public void update() {
        charConstituents.forEach(this::updateBoundingBox);
        graphicConstituents.forEach(this::updateBoundingBox);
    }

    public ArrayList<CharGlyph> getCharConstituents() {
        return charConstituents;
    }

    public ArrayList<GraphicGlyph> getGraphicConstituents() {
        return graphicConstituents;
    }
}
