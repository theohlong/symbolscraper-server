package org.dprl.symbolscraper.TrueBox.Domain;

public class MetaData implements Cloneable{
    int lineCount;
    int wordCount;
    int characterCount;
    public BBOX pdfBoundingBox;

    public MetaData(int lineCount, int wordCount, int characterCount, BBOX pdfBoundingBox){
        this.lineCount =lineCount;
        this.wordCount =wordCount;
        this.characterCount = characterCount;
        this.pdfBoundingBox = pdfBoundingBox;

    }

    @Override
    protected Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return new MetaData(
                    this.lineCount,
                    this.wordCount,
                    this.characterCount,
                    this.pdfBoundingBox);
        }
    }
}
