package org.dprl.symbolscraper.TrueBox.Domain;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.Boundable;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;

import java.util.ArrayList;

public class Line extends Boundable {
    int lineId;
    protected ArrayList<Word> words;
    public BaseLine baseLine;

    public Line(int lineId, BaseLine baseline, ArrayList<Word> words){
        this.lineId =lineId;
        this.baseLine=baseline;
        this.words=words;
    }

    @Override
    protected Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            Line l =  new Line(
                    this.lineId,
                    (BaseLine) this.baseLine.clone(),
                    (ArrayList<Word>) this.words.clone());
            l.boundingBox = boundingBox;
            return l;
        }
    }

    public Element asXml(Config c) {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("Line");
        if(c.fields.writeFields.writeLineBBOX)
            root.addAttribute("BBOX", this.boundingBox.startX + " " +
                                        +this.boundingBox.startY + " " +
                                        +this.boundingBox.width + " " +
                                        +this.boundingBox.height);

        if(c.fields.writeFields.writeId)
            root.addAttribute("id", this.lineId+"");
        return root;
    }

    public ArrayList<Word> getWords() {
        return words;
    }

    public void setWords(ArrayList<Word> words) {
        this.words = words;
    }

    @Override
    public void consume(Boundable boundable, boolean update) {
        if (boundable instanceof Word) {
            Word w = (Word) boundable;
            words.add(w);
        } else if (boundable instanceof Char) {
            // PathWrappers are constituents of words so a new word must be created to contain it
            Char c = (Char) boundable;
            ArrayList<PathWrapper> loneCharItem = new ArrayList<>();
            loneCharItem.add(c);
            // id is wrong
            Word w = new Word(10, loneCharItem, c.value);
            w.update();
        } else if (boundable instanceof Graphic) {
            Graphic g = (Graphic) boundable;
            ArrayList<PathWrapper> loneGraphicsItem = new ArrayList<>();
            loneGraphicsItem.add(g);
            // id is wrong
            Word w = new Word(10, loneGraphicsItem, g.descriptor);
            w.update();
        }
        if (update) update();
    }

    @Override
    public void update() {
        this.words.forEach(this::updateBoundingBox);
    }
}
