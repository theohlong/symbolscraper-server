package org.dprl.symbolscraper.TrueBox.Domain.Interfaces;

import java.awt.geom.GeneralPath;

public interface IGeneralPathType<A> {

    boolean intersect(A pathType);

    void setPath(GeneralPath path);
}
