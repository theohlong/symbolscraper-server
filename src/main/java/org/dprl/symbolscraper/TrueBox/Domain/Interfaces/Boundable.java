package org.dprl.symbolscraper.TrueBox.Domain.Interfaces;

import org.dprl.symbolscraper.TrueBox.Domain.BBOX;

public abstract class Boundable implements IBoundable<Boundable>, Cloneable, IXMLWriteable {

    protected BBOX boundingBox;

    public void updateBoundingBox(Boundable b) {
        
        if (this.boundingBox == null) {
            this.boundingBox = b.boundingBox;
            return;
        }
        
        this.boundingBox = this.boundingBox.updateBoundingBox(b.boundingBox);
    }

    public boolean contain(Boundable b) {
       return this.boundingBox.contain(b.boundingBox);
    }

    public boolean overlap(Boundable b) {
        return this.boundingBox.overlap(b.boundingBox);
    }

    public float intersectionOverUnion(Boundable b) {
        return  this.boundingBox.intersectionOverUnion(b.boundingBox);
    }

    public void setBoundingBox(BBOX boundingBox) {
        this.boundingBox = boundingBox;
    }

    public BBOX getBoundingBox() {
        return boundingBox;
    }
}
