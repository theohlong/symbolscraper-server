package org.dprl.symbolscraper.TrueBox;

import org.apache.pdfbox.text.TextPosition;
import org.apache.pdfbox.util.Matrix;
import org.dprl.symbolscraper.TrueBox.Domain.BBOX;

import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.io.IOException;

public class GlyphUtils {

    public static BBOX getBoundingBox(GeneralPath glyphPath) {
        PathIterator iterator = glyphPath.getPathIterator(null);

        double[] coordinates = new double[6];

        double maxX = Double.MIN_VALUE;
        double maxY = Double.MIN_VALUE;

        double minX = Double.MAX_VALUE;
        double minY = Double.MAX_VALUE;

        while(!iterator.isDone()) {
            int s = iterator.currentSegment(coordinates);

            switch (s) {
                case PathIterator.SEG_CLOSE:

                    break;

                case PathIterator.SEG_CUBICTO:

                    maxX = Math.max(maxX, coordinates[0]);
                    minX = Math.min(minX, coordinates[0]);

                    maxY = Math.max(maxY, coordinates[1]);
                    minY = Math.min(minY, coordinates[1]);

                    maxX = Math.max(maxX, coordinates[2]);
                    minX = Math.min(minX, coordinates[2]);

                    maxY = Math.max(maxY, coordinates[3]);
                    minY = Math.min(minY, coordinates[3]);

                    maxX = Math.max(maxX, coordinates[4]);
                    minX = Math.min(minX, coordinates[4]);

                    maxY = Math.max(maxY, coordinates[5]);
                    minY = Math.min(minY, coordinates[5]);
                    break;

                case PathIterator.SEG_LINETO:

                    maxX = Math.max(maxX, coordinates[0]);
                    minX = Math.min(minX, coordinates[0]);

                    maxY = Math.max(maxY, coordinates[1]);
                    minY = Math.min(minY, coordinates[1]);
                    break;

                case PathIterator.SEG_MOVETO:

                    maxX = Math.max(maxX, coordinates[0]);
                    minX = Math.min(minX, coordinates[0]);

                    maxY = Math.max(maxY, coordinates[1]);
                    minY = Math.min(minY, coordinates[1]);
                    break;

                case PathIterator.SEG_QUADTO:
                    maxX = Math.max(maxX, coordinates[0]);
                    minX = Math.min(minX, coordinates[0]);

                    maxY = Math.max(maxY, coordinates[1]);
                    minY = Math.min(minY, coordinates[1]);

                    maxX = Math.max(maxX, coordinates[2]);
                    minX = Math.min(minX, coordinates[2]);

                    maxY = Math.max(maxY, coordinates[3]);
                    minY = Math.min(minY, coordinates[3]);
                    break;
            }
            iterator.next();
        }

        float startX = (float) minX;
        float startY = (float) minY;

        float width =(float) (maxX - minX);
        float height = (float) (maxY-minY);

        return new BBOX(startX, startY, width, height);
    }

    public static int getEmSquare(TextPosition charInfo) {
        int emSquare = 0;
        try {
            emSquare = (int) (charInfo.getFont().getBoundingBox().getUpperRightX() - charInfo.getFont().getBoundingBox().getLowerLeftX());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (emSquare > 1550) { // TODO: this cutoff of 1550 works well it seems
            emSquare = 2048;
        } else {
            emSquare = 1000;
        }
        return emSquare;
    }

    public static GeneralPath translatePath(GeneralPath glyphPath, Matrix scalingMatrix, int emSquare) {

        float startX = scalingMatrix.getTranslateX();
        float startY = scalingMatrix.getTranslateY();

        AffineTransform at = new AffineTransform();
        at.translate(startX, startY);
        at.scale(scalingMatrix.getScaleX()/emSquare, scalingMatrix.getScaleY()/emSquare);

        GeneralPath newGlyph = (GeneralPath) glyphPath.clone();
        newGlyph.transform(at);
        return newGlyph;

    }
}

