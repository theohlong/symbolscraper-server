package org.dprl.symbolscraper.TrueBox;

import org.apache.fontbox.FontBoxFont;
import org.apache.fontbox.ttf.TrueTypeFont;
import org.apache.pdfbox.pdmodel.font.*;
import org.apache.pdfbox.text.TextPosition;
import org.apache.pdfbox.util.Matrix;
import org.dprl.symbolscraper.TrueBox.Domain.BBOX;
import org.dprl.symbolscraper.TrueBox.Domain.CharGlyph;
import org.javatuples.Pair;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.io.IOException;

public class FontUtils {
    public static CharGlyph drawGlyphAndExtractBBox(CharGlyph character)  {

        TextPosition text = character.charInfo;
        Matrix scalingMatrix = text.getTextMatrix();
        PDFont font = text.getFont();

        // automatically determine the [likely] em square size - more accurate fine tuning per font later
        int emSquare = GlyphUtils.getEmSquare(text);
        Pair<GeneralPath, BBOX> glyphAndBBOX = getAdjustedGlyphPath(emSquare, font, text, scalingMatrix);
        GeneralPath glyphPath = glyphAndBBOX.getValue0();
        BBOX boundingBox = glyphAndBBOX.getValue1();
        character.setBoundingBox(boundingBox);
        character.setPath(glyphPath);
        return character;
    }

    private static Pair<GeneralPath, BBOX> getAdjustedGlyphPath(int emSquare, PDFont font, TextPosition text, Matrix scalingMatrix) {
        Pair<GeneralPath, Integer> glyphAndEm = null;
        try {
            if (font instanceof PDTrueTypeFont) {
                glyphAndEm = getPDTrueTypeFontGlyph(emSquare, font, text);
            } else if (font instanceof PDType1Font) {
                glyphAndEm = getPDType1FontGlyph(emSquare, font, text);
            } else if (font instanceof PDType1CFont) {
                glyphAndEm = getPDType1CFontGlyph(emSquare, font, text);
            } else if (font instanceof PDType0Font) {
                glyphAndEm = getPDType0FontGlyph(emSquare, font, text);
            } else if (font instanceof PDType3Font) {
                glyphAndEm = getPDType3FontGlyph(emSquare, font, text);
            } else {
                //PDType1Font font1 = (PDType1Font) font;
                System.out.println("Unknown font type, skipping character at Label::" + text.getUnicode() + ". Font descriptor: " + font.getFontDescriptor());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert glyphAndEm != null;
        GeneralPath glyph = glyphAndEm.getValue0();
        emSquare = glyphAndEm.getValue1();

        GeneralPath translated = GlyphUtils.translatePath(glyph, scalingMatrix, emSquare);
        BBOX boundingBox = GlyphUtils.getBoundingBox(translated);
        return new Pair<>(translated, boundingBox);
    }

    private static Pair<GeneralPath, Integer> getPDTrueTypeFontGlyph(int emSquare, PDFont font, TextPosition text) throws IOException {
        PDTrueTypeFont TTFont = (PDTrueTypeFont) font;
        emSquare = TTFont.getTrueTypeFont().getHeader().getUnitsPerEm();
        TTFont.getPath(text.getCharacterCodes()[0]);
        return new Pair<>(TTFont.getPath(text.getCharacterCodes()[0]), emSquare);
    }

    private static Pair<GeneralPath, Integer> getPDType1FontGlyph(int emSquare, PDFont font, TextPosition text) throws IOException {
        PDType1Font Type1font = (PDType1Font) font;
        FontBoxFont FBFont = Type1font.getFontBoxFont();

        GeneralPath glyph;
        try {
            emSquare = ((TrueTypeFont) FBFont).getUnitsPerEm();
        } catch (ClassCastException | IOException cce) {
            // this emSquare setting works for some Type 1 fonts, others it doesn't
            // for those it doesn't work for, it's OK, the above general check is accurate
        }

        try {
            // TODO: this is for diacritics. Not sure if it's currently doing anything. Investigate later
            GeneralPath diacriticpath = (GeneralPath) Type1font.getPath(Type1font.getGlyphList().codePointToName(text.getUnicode().codePointAt(1)).replace("cmb", "")).clone();
            GeneralPath basepath = (GeneralPath) Type1font.getPath(Type1font.codeToName(text.getCharacterCodes()[0])).clone();
            Rectangle bounds = diacriticpath.getBounds();
            Rectangle bounds2 = basepath.getBounds();
            boolean descendingDiacritic = false;
            if (bounds.y < bounds2.y) {
                descendingDiacritic = true;
            }
            if (Character.isLowerCase(text.getUnicode().codePointAt(0)) || descendingDiacritic) { //lowercase or decending accent
                AffineTransform transform = new AffineTransform();
                basepath.append(diacriticpath.getPathIterator(transform), true);
                glyph = basepath;

            } else { // upper case with ascending accent
                AffineTransform transform = new AffineTransform();
                //                        float margin = PDfont.getFontDescriptor().getFontBoundingBox().getUpperRightY() - (bounds.y + bounds.height);
                float margin = bounds.y - font.getFontDescriptor().getXHeight();
                float newy = -bounds.y + font.getFontDescriptor().getAscent() + margin;
                //                          float newy = -bounds.y + PDfont.getFontDescriptor().getAscent() + 90 + bounds.height/2;
                transform.translate(0, newy);
                basepath.append(diacriticpath.getPathIterator(transform), true);
                glyph = basepath;

            }
        } catch (Exception e) {
            glyph = Type1font.getPath(Type1font.codeToName(text.getCharacterCodes()[0]));
        }

        return new Pair<>(glyph, emSquare);
    }

    private static Pair<GeneralPath, Integer> getPDType1CFontGlyph(int emSquare, PDFont font, TextPosition text ) throws IOException {
        PDType1CFont Type1CFont = (PDType1CFont) font;
        return new Pair<>(Type1CFont.getPath(Type1CFont.codeToName(text.getCharacterCodes()[0])), emSquare);
    }

    private static Pair<GeneralPath, Integer> getPDType0FontGlyph(int emSquare, PDFont font, TextPosition text) throws IOException {
        PDType0Font Type0font = (PDType0Font) font;
        PDCIDFont descendant = Type0font.getDescendantFont();
        int emsize = 1000;
        if (descendant instanceof PDCIDFontType2) {
            emsize = ((PDCIDFontType2) descendant).getTrueTypeFont().getHeader().getUnitsPerEm();
        }
        return new Pair<>(Type0font.getPath(text.getCharacterCodes()[0]), emsize);
    }

    private static Pair<GeneralPath, Integer> getPDType3FontGlyph(int emSquare, PDFont font, TextPosition text) throws IOException {
        // TODO: an attempt at making Type 3 work. It still doesn't, unfortunately. Future work
        // currently makes tiny little boxes in the bottom left corners of characters
        PDType1Font maskFont = PDType1Font.TIMES_ROMAN;
        return  new Pair<>(maskFont.getPath(maskFont.getName()), emSquare);
    }
}
