/******************************************************************************
* read.java
*
* Copyright (c) 2018, 2019
* Ritvik Joshi, Parag Mali, Puneeth Kukkadapu, Mahshad Mahdavi, and 
* Richard Zanibbi
*
* Document and Pattern Recognition Laboratory
* Rochester Institute of Technology, USA
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
******************************************************************************/

package org.dprl.symbolscraper.TrueBox;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Function;

public class SymbolScrapperTextStripper{

    ArrayList<PageStructure> allPages = new ArrayList<>();
    PDDocument document;
    Config conf;

    public SymbolScrapperTextStripper(PDDocument doc, Config conf) {
        document =doc;
        this.conf=conf;
    }

    public ArrayList<PageStructure> readPdf() throws IOException {
        int numPages = document.getNumberOfPages();

        for(int i=0;i<numPages;i++) {
            System.out.println("Processing page " + i);
            PageStructure currentPage = readText(i);
            allPages.add(currentPage);
        }

        return allPages;
    }

    public PageStructure readText(int pageNum) throws IOException {
        PageStructure page = new PageStructure(pageNum,new ArrayList<>(),new ArrayList<>(),null);

        BoundingBoxExtractor extractor = new BoundingBoxExtractor(document, pageNum, page);
        extractor.extract();

        for (String pp: conf.fields.processors) {
            Function<PageStructure, PageStructure> f =conf.getPostProcessorMap().get(pp);
            page = f.apply((PageStructure) page.clone());
        }

        return page;
    }
}
