package org.dprl.symbolscraper.TrueBox;

import org.apache.pdfbox.contentstream.PDFGraphicsStreamEngine;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.image.PDImage;
import org.dprl.symbolscraper.TrueBox.Domain.BBOX;
import org.dprl.symbolscraper.TrueBox.Domain.Graphic;
import org.dprl.symbolscraper.TrueBox.Domain.GraphicGlyph;


import java.awt.geom.*;
import java.io.IOException;
import java.util.ArrayList;
import java.lang.Math;
import java.util.Collections;

class GraphicsItemExtractor extends PDFGraphicsStreamEngine {

    private final GeneralPath linePath = new GeneralPath();
    private int clipWindingRule = -1;
    PDPage page;
    private int graphicId = 0;
    private int graphicGlyphId = 0;
    private final ArrayList<Graphic> allGraphics = new ArrayList<>();

    private Float xMin = null;
    private Float yMin = null;
    private Float xMax = null;
    private Float yMax = null;
    private String descriptor = "";

    private ArrayList<Point2D> points = new ArrayList<>();


    protected GraphicsItemExtractor(PDPage page) {
        super(page);
        this.page=page;
    }

    public ArrayList<Graphic> extractGraphicsItems() throws IOException {
        processPage(page);
        return allGraphics;
    }

    @Override
    public void appendRectangle(Point2D p0, Point2D p1, Point2D p2, Point2D p3) throws IOException
    {
        descriptor = descriptor + "-rectangle-";
        updateExtrema((float) p0.getX(), (float) p0.getY());
        updateExtrema((float) p1.getX(), (float) p1.getY());
        updateExtrema((float) p2.getX(), (float) p2.getY());
        updateExtrema((float) p3.getX(), (float) p3.getY());

        linePath.moveTo((float) p0.getX(), (float) p0.getY());
        linePath.lineTo((float) p1.getX(), (float) p1.getY());
        linePath.lineTo((float) p2.getX(), (float) p2.getY());
        linePath.lineTo((float) p3.getX(), (float) p3.getY());

        linePath.closePath();
    }

    @Override
    public void drawImage(PDImage pdi) throws IOException
    {
    }

    @Override
    public void clip(int windingRule) throws IOException
    {
        clipWindingRule = windingRule;
    }

    private void updateExtrema(float x, float y) {

        float lw = getGraphicsState().getLineWidth();
        float lwh = lw/2;

        points.add(new Point2D.Float(x,y));

        if (xMax == null) xMax = x + lwh;
        if (xMin == null) xMin = x - lwh;
        if (yMin == null) yMin = y - lwh;
        if (yMax == null) yMax = y + lwh;

        if (x > xMax) xMax = x + lwh;
        if (x < xMin) xMin = x - lwh;
        if (y < yMin) yMin = y - lwh;
        if (y > yMax) yMax = y + lwh;
    }

    private void captureGraphicsItem() {
        if ((xMin != null) && (yMin != null) && (xMax != null) && (yMax != null)) {
            float startX = xMin;
            float startY = yMin;
            float width = xMax - xMin;
            float height = yMax - yMin;

            GraphicGlyph b = new GraphicGlyph(graphicGlyphId,
                    new BBOX(startX, startY, width, height),
                    (GeneralPath)linePath.clone(),
                    descriptor,
                    points);

            Graphic newGraphic = new Graphic(b, descriptor, graphicId);
            allGraphics.add(newGraphic);
            graphicId++;
            graphicGlyphId++;
            resetExtrema();
        }
        points = new ArrayList<>();
    }

    private void resetExtrema() {
        xMin = null;
        yMin = null;
        xMax = null;
        yMax = null;
        descriptor = "";
    }

    @Override
    public void moveTo(float x, float y) throws IOException
    {
        linePath.moveTo(x, y);
        updateExtrema(x, y);
    }

    @Override
    public void lineTo(float x, float y) throws IOException
    {
        descriptor = descriptor + GraphicGlyph.graphicsLine;
        updateExtrema(x, y);
        linePath.lineTo(x, y);
    }

    @Override
    public void curveTo(float x1, float y1, float x2, float y2, float x3, float y3) throws IOException
    {
        Point2D curPoint = this.getCurrentPoint();
        descriptor = descriptor + "-curve-";
        ArrayList<Float> bezierExtrema = getBezierExtrema(
                (float)curPoint.getX(), (float)curPoint.getY(),
                x1, y1,
                x2, y2,
                x3, y3);

        updateExtrema(bezierExtrema.get(0), bezierExtrema.get(1));
        updateExtrema(bezierExtrema.get(0), bezierExtrema.get(3));
        updateExtrema(bezierExtrema.get(2), bezierExtrema.get(1));
        updateExtrema(bezierExtrema.get(2), bezierExtrema.get(1));
        linePath.curveTo(x1, y1, x2, y2, x3, y3);
    }

    @Override
    public Point2D getCurrentPoint()
    {
        return linePath.getCurrentPoint();
    }

    @Override
    public void closePath() {
        captureGraphicsItem();
        linePath.closePath();
    }

    @Override
    public void endPath() {
        if (clipWindingRule != -1)
        {
            linePath.setWindingRule(clipWindingRule);

            clipWindingRule = -1;
        }

        captureGraphicsItem();
        linePath.reset();
    }

    @Override
    public void strokePath() {
        captureGraphicsItem();
        linePath.reset();
    }

    @Override
    public void fillPath(int windingRule) {
        captureGraphicsItem();
        linePath.reset();
    }

    @Override
    public void fillAndStrokePath(int windingRule) {
        captureGraphicsItem();
        linePath.reset();
    }

    @Override
    public void shadingFill(COSName cosn) {
        captureGraphicsItem();
    }

    private ArrayList<Float> getBezierExtrema(float initX, float initY, float x1, float y1, float x2, float y2, float x3, float y3) {

        // Logic for the following computation derived from:
        // https://stackoverflow.com/questions/2587751/an-algorithm-to-find-bounding-box-of-closed-bezier-curves

        ArrayList<Float> tValues = new ArrayList<Float>();
        ArrayList<ArrayList<Float>> bounds = new ArrayList<>();
        bounds.add(0, new ArrayList<>());
        bounds.add(1, new ArrayList<>());

        float a, b, c, t, t1, t2, b2ac, sqrtb2ac;
        for (var i = 0; i < 2; ++i) {
            if (i == 0) {
                b = 6 * initX - 12 * x1 + 6 * x2;
                a = -3 * initX + 9 * x1 - 9 * x2 + 3 * x3;
                c = 3 * x1 - 3 * initX;
            } else {
                b = 6 * initY - 12 * y1 + 6 * y2;
                a = -3 * initY + 9 * y1 - 9 * y2 + 3 * y3;
                c = 3 * y1 - 3 * initY;
            }

            if (Math.abs(a) < 1e-12) // Numerical robustness
            {
                if (Math.abs(b) < 1e-12) // Numerical robustness
                {
                    continue;
                }
                t = -c / b;
                if (0 < t && t < 1)
                {
                    tValues.add(t);
                }
                continue;
            }
            b2ac = b * b - 4 * c * a;
            sqrtb2ac = (float) Math.sqrt(b2ac);
            if (b2ac < 0) {
                continue;
            }
            t1 = (-b + sqrtb2ac) / (2 * a);
            if (0 < t1 && t1 < 1)
            {
                tValues.add(t1);
            }
            t2 = (-b - sqrtb2ac) / (2 * a);
            if (0 < t2 && t2 < 1)
            {
                tValues.add(t2);
            }
        }

        float x, y, j = tValues.size(),
            jlen = j,
                mt;


        while (j-- > 0)
        {
            t = tValues.get((int)j);
            mt = 1 - t;
            x = (mt * mt * mt * initX) + (3 * mt * mt * t * x1) + (3 * mt * t * t * x2) + (t * t * t * x3);
            ArrayList<Float> xBounds = bounds.get(0);
            xBounds.add(x);

            y = (mt * mt * mt * initY) + (3 * mt * mt * t * y1) + (3 * mt * t * t * y2) + (t * t * t * y3);
            bounds.get(1).add(y);

        }

        bounds.get(0).add((int)jlen, initX);
        bounds.get(1).add((int)jlen, initY);
        bounds.get(0).add((int)jlen + 1, x3);
        bounds.get(1).add((int)jlen + 1, y3);


        ArrayList<Float> extrema = new ArrayList<>();
        extrema.add(0, Collections.min(bounds.get(0))); // left
        extrema.add(1, Collections.min(bounds.get(1))); // top
        extrema.add(2,Collections.max(bounds.get(0))); // right
        extrema.add(3,Collections.max(bounds.get(1))); // bottom

        return extrema;
    }
}

