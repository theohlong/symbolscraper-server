/******************************************************************************
* Main.java
*
* Copyright (c) 2018, 2019
* Ritvik Joshi, Parag Mali, Puneeth Kukkadapu, Mahshad Mahdavi, and 
* Richard Zanibbi
*
* Document and Pattern Recognition Laboratory
* Rochester Institute of Technology, USA
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
******************************************************************************/

package org.dprl.symbolscraper.TrueBox;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import java.io.*;
import java.util.ArrayList;

import net.sourceforge.argparse4j.inf.Namespace;
import org.dprl.config.ArgumentParser;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.PageStructure;

public class TrueBox {


    public TrueBox() {

    }

    public static void run(String args[]) throws IOException {

        Config c = new Config();
        Namespace parsedArgs = ArgumentParser.parseArgs(args, c);
        BufferedWriter out = getWriter(parsedArgs);
        PDDocument doc = getDocument(parsedArgs);
        SymbolScrapperTextStripper reader = new SymbolScrapperTextStripper(doc, c);
        ArrayList<PageStructure> pages = reader.readPdf();
        String xml = generateXML(pages, c, doc.getDocumentInformation() );
        out.write(xml);
        out.flush();
        if (parsedArgs.getString("boundingbox_output") != null) {
            drawPdf(doc, pages, parsedArgs, c);
        }
    }
    

    public static BufferedWriter getWriter(Namespace parsedArgs) {
        BufferedWriter out = null;
        String dest = parsedArgs.getString("output");
        System.out.println(dest);
        try {
            if (dest == null) {
                out = new BufferedWriter(new OutputStreamWriter(System.out));
            } else {
                out = new BufferedWriter(new FileWriter(dest));
            }
        } catch (IOException e) {
            System.out.println("Couldn't open output file: " + parsedArgs.getString("output"));
            System.exit(1);
        }
        return out;
    }

    public static PDDocument getDocument(Namespace parsedArgs) throws IOException {
        File initialFile = new File(parsedArgs.getString("file"));
        InputStream pdfInputStream = new FileInputStream(initialFile);
        return PDDocument.load(pdfInputStream);
    }

    public static String generateXML(ArrayList<PageStructure> pages, Config conf, PDDocumentInformation docInfo ) throws IOException {
        SymbolScraperXmlWriter display = new SymbolScraperXmlWriter(pages, conf, docInfo);
        return display.writePDF();
    }

    public static void drawPdf(PDDocument document, ArrayList<PageStructure> pages, Namespace parsedArgs, Config conf) throws IOException {
        BoundingBoxDrawer box = new BoundingBoxDrawer(
                document,
                pages,
                parsedArgs.getString("boundingbox_output"),
                parsedArgs.getString("file"),
                conf);

        for (int i = 0; i < pages.size(); i++) {
            box.draw(i);
        }
        box.writePdf();
    }
}


