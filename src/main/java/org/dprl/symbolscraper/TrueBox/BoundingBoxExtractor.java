package org.dprl.symbolscraper.TrueBox;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.dprl.symbolscraper.TrueBox.Domain.PageStructure;

import java.io.IOException;

public class BoundingBoxExtractor {
    PDDocument document;
    int pageNum;
    PageStructure page;

    public int lineId;
    public int wordId;
    public int charId;

    public BoundingBoxExtractor(PDDocument document, int pageNum, PageStructure page) {
        this.document = document;
        this.pageNum = pageNum;
        this.page = page;
    }

    public void extract() throws IOException {
        GraphicsItemExtractor graphicsItemExtractor = new GraphicsItemExtractor(document.getPage(pageNum));
        page.graphics = graphicsItemExtractor.extractGraphicsItems();
        CharacterExtractor characterExtractor = new CharacterExtractor(this.document, this.pageNum, this.page);
        characterExtractor.extractCharacters();

        lineId = characterExtractor.lineId;
        wordId = characterExtractor.wordId;
        charId = characterExtractor.charId;
    }
}
