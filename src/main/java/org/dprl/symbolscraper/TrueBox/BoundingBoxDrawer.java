/******************************************************************************
* drawBBOX.java
*
* Copyright (c) 2018, 2019
* Ritvik Joshi, Parag Mali, Puneeth Kukkadapu, Mahshad Mahdavi, and 
* Richard Zanibbi
*
* Document and Pattern Recognition Laboratory
* Rochester Institute of Technology, USA
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
******************************************************************************/

package org.dprl.symbolscraper.TrueBox;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.Domain.*;
import org.dprl.symbolscraper.TrueBox.Domain.Interfaces.PathWrapper;

import java.awt.*;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;


public class BoundingBoxDrawer {
    String outFile;
    String inputFileName;
    PDDocument document;
    ArrayList<PageStructure> allPages;

    Color charColor = Color.green;
    Color charGlyphColor = Color.pink;
    Color graphicColor = Color.red;

    Color pointColor = Color.magenta;
    Color wordColor = Color.cyan;
    Color lineColor = Color.orange;
    Color pageContentColor = Color.yellow;

    Config conf;

    public BoundingBoxDrawer(PDDocument doc, ArrayList<PageStructure> allPages, String outFile, String InpFile, Config conf){
        this.outFile = outFile;
        this.inputFileName = InpFile;
        this.allPages=allPages;
        this.document=doc;
        this.conf = conf;
    }

    public void draw(int pageNum) throws IOException {

        PDPage page = document.getPage(pageNum);
        PDPageContentStream contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, false);

        PageStructure curPage = allPages.get(pageNum);

        if (conf.fields.drawFields.drawPageBorderBBOX) {
            contentStream.addRect(page.getBBox().getLowerLeftX(), page.getBBox().getLowerLeftY(), page.getBBox().getWidth(), page.getBBox().getHeight());
            contentStream.setStrokingColor(Color.magenta);
            contentStream.stroke();
        }

        if (conf.fields.drawFields.drawContentBBOX) addRect(contentStream, curPage.getBoundingBox(), pageContentColor);
        for (Line l: curPage.lines) {
            if (conf.fields.drawFields.drawLineBBOX) addRect(contentStream, l.getBoundingBox(), lineColor);
            for (Word w: l.getWords()) {
                if (conf.fields.drawFields.drawWordBBOX) addRect(contentStream, w.getBoundingBox(), wordColor);
                for (PathWrapper wordEntry: w.getConstituents()) {
                    if (wordEntry instanceof Char) {
                        Char c = (Char) wordEntry;
                        if (conf.fields.drawFields.drawCharBBOX) addRect(contentStream, c.getBoundingBox(), charColor);
                        if (conf.fields.drawFields.drawCharGlyphBBOX) {
                            for (CharGlyph cg : c.getCharConstituents()) {
                                addRect(contentStream, cg.getBoundingBox(), charGlyphColor);
                            }
                        }
                         if (conf.fields.drawFields.drawGraphicBBOX) {
                             for (GraphicGlyph gi: c.getGraphicConstituents()) {
                                 addRect(contentStream, gi.getBoundingBox(), graphicColor);
                             }
                         }

                         if (conf.fields.drawFields.drawPoints) {
                             for (GraphicGlyph gi: c.getGraphicConstituents()) {
                                 System.out.print(conf.fields.drawFields.drawPoints);
                                 for (Point2D p: gi.points)
                                    addPoint(contentStream, p, pointColor);
                             }
                         }
                    } else if (wordEntry instanceof Graphic) {
                        if (conf.fields.drawFields.drawGraphicBBOX) addRect(contentStream, wordEntry.getBoundingBox(), graphicColor);
                        if (conf.fields.drawFields.drawPoints) {
                            Graphic g = (Graphic) wordEntry;
                            for (GraphicGlyph gi: g.getGraphicConstituents()) {
                                System.out.print(conf.fields.drawFields.drawPoints);
                                for (Point2D p: gi.points)
                                    addPoint(contentStream, p, pointColor);
                            }
                         }
                    }
                }
            }
        }

        if (conf.fields.drawFields.drawGraphicBBOX)
            for(Graphic gi: curPage.graphics) {
                addRect(contentStream, gi.getBoundingBox(), graphicColor);
            }
        if (conf.fields.drawFields.drawPoints)
            for(Graphic gi: curPage.graphics)
                for (GraphicGlyph g: gi.getGraphicConstituents())
                    for (Point2D p: g.points)
                        addPoint(contentStream, p, pointColor);

        contentStream.closeAndStroke();
        contentStream.close();
    }

    public void addRect(PDPageContentStream contentStream, BBOX boundingBox, Color color) throws IOException {
        if (boundingBox == null) return;
        contentStream.addRect(boundingBox.startX, boundingBox.startY, boundingBox.width, boundingBox.height);
        contentStream.setLineWidth(0.2f);
        contentStream.setStrokingColor(color);
        contentStream.stroke();
    }

    public void addPoint(PDPageContentStream contentStream, Point2D point, Color color) throws IOException {
        contentStream.addRect((float) (point.getX()-.25), (float) (point.getY()-.25), 0.5f, 0.5f);
        contentStream.setLineWidth(0.2f);
        contentStream.setStrokingColor(color);
        contentStream.stroke();
    }

    public void writePdf() {
        try {
            //FileOutputStream outputStream = new FileOutputStream(outFile);
            File outputFile = new File(outFile);
            document.save(outputFile);
            document.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
