package org.dprl.server;
import io.javalin.http.Context;
import io.javalin.http.UploadedFile;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

import org.dprl.config.Config;
import org.dprl.symbolscraper.TrueBox.*;
import org.dprl.symbolscraper.TrueBox.Domain.PageStructure;
import org.dprl.symbolscraper.TrueBox.SymbolScrapperTextStripper;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;

public class SymbolScraperController {


    public static void drawPdf(Context ctx) throws IOException {
        UploadedFile pdf = ctx.uploadedFile("pdf");

        assert pdf != null;
        Config conf = new Config();
        conf.applyQueryParams(ctx.queryParamMap());
        InputStream pdfInputStream = pdf.getContent();
        PDDocument document = PDDocument.load(pdfInputStream);
        SymbolScrapperTextStripper reader = new SymbolScrapperTextStripper(document, conf);
        ArrayList<PageStructure> allPages = reader.readPdf();
        String baseOutDir = System.getProperty("java.io.tmpdir");
        String curTime = getTime();
        Path outDir = Paths.get(baseOutDir, curTime);
        String inpFilename = "temp";
        BoundingBoxDrawer box = new BoundingBoxDrawer(document, allPages, outDir.toString(), inpFilename, conf);


        for (int pageI = 0; pageI < allPages.size(); pageI++) {
            box.draw(pageI);
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        document.save(outputStream);
        ByteArrayInputStream inStream = new ByteArrayInputStream( outputStream.toByteArray());
        ctx.result(inStream);
        ctx.header("Content-Disposition", "attachment; filename=test.pdf");
        ctx.status(200);
    }

    public static void processPdf(Context ctx) throws IOException {

        // /*
        UploadedFile pdf = ctx.uploadedFile("pdf");
        assert pdf != null;
        Config conf = new Config();
        conf.applyQueryParams(ctx.queryParamMap());
        InputStream pdfInputStream = pdf.getContent();
        PDDocument document = PDDocument.load(pdfInputStream);
        SymbolScrapperTextStripper reader = new SymbolScrapperTextStripper(document, conf);
        ArrayList<PageStructure> allPages = reader.readPdf();
        SymbolScraperXmlWriter display = new SymbolScraperXmlWriter(allPages, conf, document.getDocumentInformation() );
        String ret = display.writePDF();
        ctx.result(ret);
        // */
    }

    public static String getTime() {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        return dateFormat.format(date);
    }
}
