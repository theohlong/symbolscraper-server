package org.dprl;

import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.annotations.HttpMethod;
import io.javalin.plugin.openapi.dsl.*;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;
import org.dprl.config.Config;
import org.dprl.server.ErrorResponse;
import org.dprl.server.SymbolScraperController;
import org.dprl.symbolscraper.TrueBox.TrueBox;

import java.io.IOException;
import java.io.InputStream;


import static io.javalin.apibuilder.ApiBuilder.*;

public class Main {
    public static void main(String[] args) {
	java.util.logging.Logger.getLogger("org.apache.pdfbox")
		.setLevel(java.util.logging.Level.OFF);
        if (args.length > 0) {
            try {
                TrueBox.run(args);
            } catch (IOException e) {
                System.exit(1);
            }
        } else {
            runServer();
        }
    }

    private static void runServer() {
        Javalin.create(config -> {
            config.registerPlugin(getConfiguredOpenApiPlugin());
            //config.defaultContentType = "application/json";
        }).routes(() ->  {
            path("process-pdf", () ->  post(SymbolScraperController::processPdf));
            path("draw-pdf", () -> post(SymbolScraperController::drawPdf));
        })
                .start(7002);

        System.out.println("Check out ReDoc docs at http://localhost:7002/redoc");
        System.out.println("Check out Swagger UI docs at http://localhost:7002/swagger-ui");
    }


    private static OpenApiPlugin getConfiguredOpenApiPlugin() {
        Info info = new Info().version("1.0").description("User API");
        OpenApiOptions options = new OpenApiOptions(info)
                .setDocumentation("process-pdf", HttpMethod.POST, getDocumentation())
                .setDocumentation("draw-pdf", HttpMethod.POST, getDrawDocumentation())
                .activateAnnotationScanningFor("")
                .path("/swagger-docs") // endpoint for OpenAPI json
                .swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
                .reDoc(new ReDocOptions("/redoc")) // endpoint for redoc
                .defaultDocumentation(doc -> {
                    doc.json("500", ErrorResponse.class);
                    doc.json("503", ErrorResponse.class);
                });
        return new OpenApiPlugin(options);
    }

    private static OpenApiDocumentation getDocumentation() {
        return OpenApiBuilder.document()
                .operation(openApiOperation -> {
                    openApiOperation.description("Process PDF");
                    openApiOperation.operationId("myOperationId");
                    openApiOperation.summary("Extract symbols from the pdf, apply post-processors and return the information in XML format");
                    openApiOperation.deprecated(false);
                    openApiOperation.addTagsItem("user");
                })
                .queryParam("Fields", Config.Fields.WriteFields.class)
                .uploadedFile("pdf")

                .html("200");
    }

    private static OpenApiDocumentation getDrawDocumentation() {
        return OpenApiBuilder.document()
                .operation(openApiOperation -> {
                    openApiOperation.description("Draw PDF");
                    openApiOperation.summary("Extract symbols from the pdf, apply post-processors and return the information in PDF form with bounding boxes drawn");
                    openApiOperation.deprecated(false);
                })
                .queryParam("Fields", Config.Fields.DrawFields.class)
                .uploadedFile("pdf")
                .result("200", InputStream.class, "application/pdf" );
    }
}
