package org.dprl.config;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;


public class ArgumentParser {

    public static Namespace parseArgs(String args[], Config conf) {

        net.sourceforge.argparse4j.inf.ArgumentParser parser = ArgumentParsers.newFor("SymbolScraper").build()
                .defaultHelp(true)
                .description("Extract text, math, and chemical information from pdfs");
        parser.addArgument("-f", "--file")
                .required(true)
                .help("Path to the pdf file o extract data from");
        parser.addArgument("-o", "--output")
                .required(false)
                .help("Where to place xml results. Can pass in a path to a file to generate or " +
                        "overwrite xml results. If not set defaults to console output");
        parser.addArgument("-b", "--boundingbox-output")
                .required(false)
                .help("PDF file to place pdf bounding box results. Can pass in a path to a file to generate or " +
                        "overwrite pdf results. If not set defaults to no output generated");
        parser.addArgument("--drawPageBorderBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.drawFields.drawPageBorderBBOX);
        parser.addArgument("--drawContentBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.drawFields.drawContentBBOX);
        parser.addArgument("--drawLineBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.drawFields.drawLineBBOX);
        parser.addArgument("--drawWordBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.drawFields.drawWordBBOX);
        parser.addArgument("--drawCharBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.drawFields.drawCharBBOX);
        parser.addArgument("--drawCharGlyphBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.drawFields.drawCharGlyphBBOX);
        parser.addArgument("--drawGraphicBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.drawFields.drawGraphicBBOX);
        parser.addArgument("--drawPoints")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.drawFields.drawPoints);
        // Write options
        parser.addArgument("--writePageBorderBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.writePageBorderBBOX);
        parser.addArgument("--writeContentBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.writeContentBBOX);
        parser.addArgument("--writeLinesBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.writeLineBBOX);
        parser.addArgument("--writeWordsBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.writeWordsBBOX);
        parser.addArgument("--writeCharBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.writeCharBBOX);
        parser.addArgument("--writeCharGlyphBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.writeCharGlyphBBOX);
        parser.addArgument("--writeGraphicBBOX")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.writeGraphicBBOX);
        parser.addArgument("--writeId")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.writeId);
        parser.addArgument("--writePoints")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.writePoints);

        // Char attributes

        parser.addArgument("--writeAllCharAttributes")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.charAttributes.writeAllCharAttributes);
        parser.addArgument("--writeFontName")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.charAttributes.writeFontName);
        parser.addArgument("--writeFontSize")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.charAttributes.writeFontSize);
        parser.addArgument("--writeBold")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.charAttributes.writeBold);
        parser.addArgument("--writeItalicAngle")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.charAttributes.writeItalicAngle);
        parser.addArgument("--writeColor")
                .required(false)
                .type(Boolean.class)
                .setDefault(conf.fields.writeFields.charAttributes.writeColor);

        // processors
        parser.addArgument("--processors")
                .required(false)
                .nargs("*")
                .setDefault(conf.fields.processors);


        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
            conf.applyNamespace(ns);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        return ns;
    }
}
