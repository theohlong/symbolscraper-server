package org.dprl.config;

import net.sourceforge.argparse4j.inf.Namespace;
import org.dprl.symbolscraper.TrueBox.Domain.PageStructure;
import org.dprl.symbolscraper.TrueBox.PostProcessor.MathPostProcessor;
import org.dprl.symbolscraper.TrueBox.PostProcessor.SplicePostProcessor;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class Config {

    // Inner class exist so output of OPENAPI documentation is correct.
    public class Fields {

        public Fields() {
            writeFields = new WriteFields(processors);
            drawFields = new DrawFields(processors);
        }

        public class DrawFields {

             public List<String> processors;

             public DrawFields(List<String> processors) {
                this.processors = processors;
            }

            public boolean drawPageBorderBBOX = false;
            public boolean drawContentBBOX = false;
            public boolean drawLineBBOX = false;
            public boolean drawWordBBOX = false;
            public boolean drawCharBBOX = true;
            public boolean drawCharGlyphBBOX = false;
            public boolean drawGraphicBBOX = true;

            public boolean drawPoints = false;
        }

        public class WriteFields {

            public List<String> processors;

            public WriteFields(List<String> processors) {
                this.processors = processors;
            }

            public boolean writePageBorderBBOX = false;
            public boolean writeContentBBOX = false;
            public boolean writeLineBBOX = false;
            public boolean writeWordsBBOX = false;
            public boolean writeCharBBOX = true;
            public boolean writeCharGlyphBBOX = true;
            public boolean writeGraphicBBOX = true;
            public boolean writePoints = false;
            public boolean writeId = true;

            public class CharAttributes {

                public boolean writeAllCharAttributes = false;
                public boolean writeFontName = false;
                public boolean writeFontSize = false;
                public boolean writeBold = false;
                public boolean writeItalicAngle = false;
                public boolean writeColor = false;

            }

            public CharAttributes charAttributes = new CharAttributes();

        }

        public List<String> processors = List.of(
        );

        public WriteFields writeFields;
        public DrawFields drawFields;
    }

    public Fields fields;

    private final Function<PageStructure, PageStructure> splicePostProcessor = new SplicePostProcessor();
    private final Function<PageStructure, PageStructure> mathPostProcessor = new MathPostProcessor();

    private final Map<String, Function<PageStructure, PageStructure>> processorMap = Map.of(
            "SpliceGraphicsItemsAndCharsWithIOU", splicePostProcessor,
            "IdentifyMathFromSplicedStructure", mathPostProcessor
    );


    public Config() {
        fields = new Fields();
    }

    public Map<String, Function<PageStructure, PageStructure>> getPostProcessorMap() {
        return processorMap;
    }

    public void applyQueryParams(Map<String, List<String>> queryParams) {
        try {
            setArgsFromQueryParams(queryParams, this.fields.writeFields);
            setArgsFromQueryParams(queryParams, this.fields.drawFields);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            System.out.println(e);
        }
    }


    private void setArgsFromQueryParams(Map<String, List<String>> queryParams, Object clazz) throws IllegalAccessException, NoSuchFieldException {

        Field[] fields = clazz.getClass().getFields();
        for(Field field: fields) {
            if (field.getName().equals("processors") && queryParams.containsKey("processors")) {
                setProcessorsFromList(Arrays.asList(queryParams.get(field.getName()).get(0).split(",")));
            } else if (field.getName().equals("charAttributes")) {
                setCharAttrArgsFromQueryParams(queryParams);
            } else if(queryParams.containsKey(field.getName())) {
                boolean boolVal = queryParams.get(field.getName()).get(0).toLowerCase().equals("true");
                field.set(clazz, boolVal);
            }
        }
    }

    public void setCharAttrArgsFromQueryParams(Map<String, List<String>> queryParams) throws IllegalAccessException {
        Field[] fields = this.fields.writeFields.charAttributes.getClass().getFields();
        boolean allTrue = false;
        for(Field f: fields) {
            String nestedAttr = "charAttributes["+f.getName()+"]";
            if(queryParams.containsKey(nestedAttr)) {
                if (nestedAttr.equals("charAttributes[writeAllCharAttributes]")) {
                    allTrue = queryParams.get(nestedAttr).get(0).toLowerCase().equals("true");
                }
                boolean boolVal = queryParams.get(nestedAttr).get(0).toLowerCase().equals("true") || allTrue;
                f.set(this.fields.writeFields.charAttributes, boolVal);
            }
        }
    }


    public void applyNamespace(Namespace parsedArgs) {
        try {
            setArgsFromNamespace(parsedArgs, this.fields.drawFields);
            setArgsFromNamespace(parsedArgs, this.fields.writeFields);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            System.out.println(e);
        }
    }

    private void setArgsFromNamespace(Namespace parsedArgs, Object clazz) throws IllegalAccessException, NoSuchFieldException {
        Field[] fields = clazz.getClass().getFields();
        for(Field field: fields) {
            if (field.getName().equals("processors") && parsedArgs.getAttrs().containsKey("processors")) {
                setProcessorsFromList(parsedArgs.getList(field.getName()));
            } else if (field.getName().equals("charAttributes")) {
                setCharAttrArgsFromNamespace(parsedArgs);
            } else if(parsedArgs.getAttrs().containsKey(field.getName())) {
                boolean boolVal = parsedArgs.getBoolean(field.getName()).equals(true);
                field.set(clazz, boolVal);
            }
        }
    }

    private void setCharAttrArgsFromNamespace(Namespace n) throws IllegalAccessException {
        Field[] fields = this.fields.writeFields.charAttributes.getClass().getFields();

        boolean allTrue = false;

        for(Field f: fields) {
            if(n.getAttrs().containsKey(f.getName())) {
                if (f.getName().equals("writeAllCharAttributes")) {
                    allTrue = n.getBoolean("writeAllCharAttributes");
                }
                boolean boolVal = n.getBoolean(f.getName()) || allTrue;
                f.set(this.fields.writeFields.charAttributes, boolVal);
            }
        }
    }


    private void setProcessorsFromList(List<String> processors) throws IllegalAccessException, NoSuchFieldException {
        List<String> approvedProcessors = new ArrayList<>();
        if (processors.size() == 1) {
            // the Swagger api doc sets this as the default so if it isnt changed we want to set the REAL defaults
            if (processors.get(0).toLowerCase().equals("string")) {
                approvedProcessors = this.fields.processors;
            }
        }

        for (String s: processors ) {
            if (this.processorMap.containsKey(s)) {
                approvedProcessors.add(s);
            }
        }

        Field f = this.fields.getClass().getField("processors");
        f.set(this.fields, approvedProcessors);

        Field fWrite = this.fields.writeFields.getClass().getField("processors");
        fWrite.set(this.fields.writeFields, approvedProcessors);

        Field fDraw = this.fields.drawFields.getClass().getField("processors");
        fDraw.set(this.fields.drawFields, approvedProcessors);
    }
}
