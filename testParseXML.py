# Simple program to parse SymbolScraper XML
# output. Extract character entries and prints
# them to standard output.
#
# M. Langsenkamp, R. Zanibbi, Oct. 2021
from bs4 import BeautifulSoup

infile = open("xmlOutput.xml","r")
contents = infile.read()
soup = BeautifulSoup(contents,'xml')
chars = soup.find_all('Char')

for character in chars:
  print( character.prettify() )

