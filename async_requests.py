
# conda create -n asyncreq python=3.9
# conda activate asyncreq

import time
import requests
import asyncio
import functools
import argparse
import os
import subprocess

async def async_calls_docker(calls, pdf_file):

    filename = os.path.basename(pdf_file)

    r = "http://localhost:7002/process-pdf"
    loop = asyncio.get_event_loop()
    futures = []
    for i in range(calls):
        file = {"pdf": (filename, open(pdf_file, 'rb'), "application/pdf")}
        futures.append(
            loop.run_in_executor(
                None,
                functools.partial(
                    requests.post,
                    r,
                    files =file
                )
            )
        )

    for response in await asyncio.gather(*futures):
        if response.status_code != 200:
            print("request failed!")


def sync_calls_docker(calls: int, pdf_file: str):

    filename = os.path.basename(pdf_file)

    r = "http://localhost:7002/process-pdf"
    for i in range(calls):
        file = {"pdf": (filename, open(pdf_file, 'rb'), "application/pdf")}
        resp = requests.post(r, files=file)
        if resp.status_code != 200:
            print("request failed!")


def sync_calls_command_line(calls: int, pdf_file: str):
    for i in range(calls):
        subprocess.check_output(["java",  "-jar", "target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar", "-f", pdf_file])


def main():

    parser = argparse.ArgumentParser(description='Load test symbol scraper')
    parser.add_argument('--iters', type=int, default=100,
                    help='number of times to make the request')
    parser.add_argument('--file', type=str, default="src/test/resources/hellosymbscrap.pdf",
                    help='file to process')

    args = parser.parse_args()
    calls = args.iters
    file_name = args.file

    start = time.time()
    sync_calls_command_line(calls, file_name)
    end = time.time()
    print(f"time taken to run symbolscraper {calls} times sync from command line: " + str(end - start))

    start = time.time()
    sync_calls_docker(calls, file_name)
    end = time.time()
    print(f"time taken to run symbolscraper {calls} times sync from docker: " + str(end - start))

    start = time.time()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(async_calls_docker(calls, file_name))

    end = time.time()
    print(f"time taken to run symbolscraper {calls} times async from docker: " + str(end - start))

if __name__ =="__main__":
    main()