# Symbol Scraper Server (version 0.2)

Symbol Scraper extracts the locations and attributes associated with characters
and graphics in PDF files. Attributes include fonts, line endpoints, line
widths, etc.  Results are returned in XML, or as an annotated version of an input
PDF file showing the bounding box locations of identified text and graphics.

The basic strategy used in SymbolScraper is to intercept the rendering pipeline
of [Apache PDFBox](https://pdfbox.apache.org). This allows the precise
locations of objects drawn on a PDF page to be computed.  In PDF, embedded
images (e.g., PNGs) are stored as raster arrays, without drawing instructions
for their contents; as a result, characters and graphics in embedded images are
not available in SymbolScraper's XML output (this requires OCR).

Symbol Scraper can be run from the command line for individual PDFs, or as a
[Javalin web server](https://javalin.io) that can accept and process requests
asynchronously. A docker container for the Javelin server is also provided.

This new version of Symbol Scraper was redesigned and rewritten for the
[Document and Pattern Recognition Lab](https://www.cs.rit.edu/~dprl/index.html)
at RIT (USA) by Matt Langsenkamp (mattlangsenkamp@gmail.com).

**System Description and Citation.** A more detailed description of the system,
which you should cite if you use Symbol Scraper in your own work is available:

* Ayush Kumar Shah, Abhisek Dey, and Richard Zanibbi (2021). [A Math Formula Extraction and Evaluation Framework for PDF Documents](https://www.cs.rit.edu/~rlaz/files/ICDAR2021_MathSeer_Pipeline.pdf), Proc. International Conference on Document Analysis and Recognition (ICDAR).
 
To cite with BibTex use the following block
```tex
@incollection{llados_math_2021,
	address = {Cham},
	title = {A {Math} {Formula} {Extraction} and {Evaluation} {Framework} for {PDF} {Documents}},
	volume = {12822},
	isbn = {978-3-030-86330-2 978-3-030-86331-9},
	url = {https://link.springer.com/10.1007/978-3-030-86331-9_2},
	abstract = {We present a processing pipeline for math formula extraction in PDF documents that takes advantage of character information in born-digital PDFs (e.g., created using LATEX or Word). Our pipeline is designed for indexing math in technical document collections to support math-aware search engines capable of processing queries containing keywords and formulas. The system includes user-friendly tools for visualizing recognition results in HTML pages. Our pipeline is comprised of a new state-of-the-art PDF character extractor that identiﬁes precise bounding boxes for non-Latin symbols, a novel Single Shot Detectorbased formula detector, and an existing graph-based formula parser (QDGGA) for recognizing formula structure. To simplify analyzing structure recognition errors, we have extended the LgEval library (from the CROHME competitions) to allow viewing all instances of speciﬁc errors by clicking on HTML links. Our source code is publicly available.},
	language = {en},
	urldate = {2022-06-17},
	booktitle = {Document {Analysis} and {Recognition} – {ICDAR} 2021},
	publisher = {Springer International Publishing},
	author = {Shah, Ayush Kumar and Dey, Abhisek and Zanibbi, Richard},
	editor = {Lladós, Josep and Lopresti, Daniel and Uchida, Seiichi},
	year = {2021},
	doi = {10.1007/978-3-030-86331-9_2},
	note = {Series Title: Lecture Notes in Computer Science},
	pages = {19--34},
}
```
## A Note on XML Output: Post-Processors

After the initial extraction of symbols, graphics and text items live in separate data structures. 

Often we want to combine text and graphics data, for example to correctly
identify a square root symbol comprised of a radical ('checkmark') Unicode
character and a horizontal line. To combine the character and line into one
character, we can apply post-processors that match graphics and characters
based on geometric tests for bounding box overlap and the intersection of
glyphs (character outlines) and graphics.

We provide a pair of post-processors designed with math symbols in mind, which
are inactive by default (an example of using the post-processer is provided
below). 

To create a new post-processor, all that is needed is to implement the
`Function<PageStructure, PageStructure>` interface, and then add an instance in
the `Config.java` source file.

## Installation
### Requirements

 * Java 8 or higher 
 * Maven
 * Docker (for docker container version -- recommended, but not required)

### Installation Types

Symbol Scraper can be run two ways:

1. From the command line, and
2. through a web service (port 7002 by default)

For those new to Symbol Scraper, we recommend starting by installing the command line version and then skip to the command line examples below (under "Running Symbol Scraper").

For those familiar with Symbol Scraper and looking to process multiple documents quickly, you will want to set up the web service. We provide two options for this, but please note that *both* listen on port 7002, and so only one of these should be run at a given time:

1. Javelin server (started with a `java` call at the command line)
2. A docker container (which wraps a Javelin server)

We provide installation instructions for the command line and the web services below.

### Building the Command Line Program and Java Web Service 


To build the the program for use at the command line or as a Java web service,
first move to the base directory (`symbolscraper-server`), and then build the
system with maven, using the following command (tested on linux
systems with java 11 and java 14):

```shell script
mvn clean package
```
The program now resides in a shaded jar (i.e., a compiled, linked set of jar files) located at `target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar`. 

Because java is a compiled language, if you make any
changes to the code, you will have to rebuild the shaded jar using `mvn` to see
the effects of your edits.

**If you are new to Symbol Scraper,** we recommend that you jump ahead to the command line examples under "Running Symbol Scraper" now.

#### Local Java Web Service (with Javelin)

After successfully compiling the shaded jar, you may start the web server using the following command:

```shell script
java -jar ./target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar
```

This will run Symbol Scraper as a server on port 7002.

At this point, you will see the server start up and produce this output:

```
       __                      __ _            __ __
      / /____ _ _   __ ____ _ / /(_)____      / // /
 __  / // __ `/| | / // __ `// // // __ \    / // /_
/ /_/ // /_/ / | |/ // /_/ // // // / / /   /__  __/
\____/ \__,_/  |___/ \__,_//_//_//_/ /_/      /_/

          https://javalin.io/documentation

23:01:38.221 [main] INFO  io.javalin.Javalin - Starting Javalin ...
23:01:38.236 [main] INFO  io.javalin.Javalin - You are running Javalin 4.0.1 (released September 25, 2021).
23:01:38.398 [main] INFO  io.javalin.Javalin - Listening on http://localhost:7002/
23:01:38.398 [main] INFO  io.javalin.Javalin - Javalin started in 177ms \o/
Check out ReDoc docs at http://localhost:7002/redoc
Check out Swagger UI docs at http://localhost:7002/swagger-ui
```

You can then go to the swagger docs URL ([http://localhost:7002/swagger-ui](http://localhost:7002/swagger-ui)) to interact with the server (e.g., passing PDF files to check XML output).  Currently, it is better to use the `curl` commands below to obtain the PDF bounding box visualizations.

`ctrl-c` should stop the service in linux/unix shells.

### Docker Installation

If you have the docker daemon installed and running on your machine, issue:

```shell script
docker run -p 7002:7002 dprl/symbolscraper-server:latest
```

<!--where **&lt;tag&gt;** is one of `dev`, `staging`, or `prod`. -->

The system is then ready to process requests. 

After installing the docker container as described above, point your web browser to [http://localhost:7002/swagger-ui#](http://localhost:7002/swagger-ui#). This is the same interface that the Javelin server creates, as the docker container essentially wraps the Javelin server.

**Development Note:** This note is for those looking to modify the docker container for their own purposes (the one provided should work as-is, most people reading can skip this).

From the top level directory of Symbol Scraper, to rebuild the docker container on your machine, make sure to use a tag (&lt;sometag&gt;) other than `dev`. You can use this command to build a new  docker image:

```shell script
docker build -t symbolscraper-server:<sometag> . 
```
where `.` indicates that `Dockerfile` in the current directory should be used to specify the new docker container environment and parameters.

## Running Symbol Scraper 

### Command Line Execution

*First Example.* In the top-level directory, you will find the PDF `words.pdf`, which 
contains the phrase "The quick brown fox jumped over the lazy dog", and a page
number. To extract the character and page number locations, run the following
command:

```shell script
java -jar target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar -f words.pdf -b bounded.pdf
```

This command prints XML output to the terminal (standard output), and saves a visualization
PDF `bounded.pdf` to the current directory.

*Larger Example with Diagrams.* As another example with a larger PDF file, let's analyze the `largerExample.pdf` file, which contains 5 pages of text, math, and chemical diagrams -- the first page is intentionally blank, containing only a non-visible line and rectangle. To analyze this file and save both the XML and PDF bounding box visualization to disk as `./largerSymbols.xml` and `./largerBBs.pdf`, run the following:

```shell script
java -jar target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar -f largerExample.pdf -o largerSymbols.xml -b largerBBs.pdf --processors SpliceGraphicsItemsAndCharsWithIOU IdentifyMathFromSplicedStructure
``` 
This makes use of two XML data **post-processing steps** named at the end of the command. These commands merge touching symbols (e.g., to identify square roots from radical characters and touching horizontal lines). You can run the command again without post-processing to see the difference for the page with math. 

The pages of `largerExample.pdf` may also be run independently: `src/test/resources` contains separate PDFs for each page. 

*General Usage.* More generally, to run Symbol Scraper at the command line, you can use:

```
java -jar target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar 
``` 
followed by the appropriate combination of the main command line options:

*  `-f`: input PDF file (REQUIRED)
*  `-o`: write output XML to given file name. If missing, XML written to standard output. 
*  `-b`: write PDF visualization of bounded boxes to given file name. If missing, no PDF visualization is produced.
*  `--processors` run one or more post-processors listed (currently, only the two used in the second example above are defined).

#### More on Command Line Options

Symbol Scraper is highly configurable, with additional command-line options to control which bounding boxes are drawn in PDFs, and what attributes are written to XML.  The full list of configurable parameters can be found in `src/main/java/org/dprl/config/Config.java`, or can be seen by issuing:

```shell script
java -jar target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar --help
```

If an option is prefaced with "draw" it controls what is drawn in the visualization PDFs created in the directory specified by the `-b` flag. Options prefaced by "write" control which attributes are written to the XML output (put in the file indicated by `-o`, or written to standard output). 

*Word Boxes.* As an example of drawing PDFs with only words bounding boxes, let us re-run `./words.pdf` using the following command, with the visualization PDF again written to `./bounded.pdf`. 

```shell script
java -jar target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar -f words.pdf -b bounded.pdf --drawGraphicBBOX false --drawCharBBOX false --drawWordBBOX true
```
Note that for some options, the default value is `true,` so we must set them to false. Because the `-o` flag is missing, XML is printed to standard output.


*Font Names.* In this example we run the example math page on its own, and write the font name for each character in the XML output (written to standard output). The post processors for math symbols described early are also run in this example.

```shell script
java -jar target/symbolscraper-server-1.0-SNAPSHOT-shaded.jar -f src/test/resources/math.pdf --writeFontName true --processors SpliceGraphicsItemsAndCharsWithIOU IdentifyMathFromSplicedStructure
```







### Running the Web Service

The Symbol Scraper web services allow files to processed more quickly, and are designed to process asynchronous requests.

After installation (see above for details), the web service can be run in the following ways:

1. Interactively, using [http://localhost:7002/swagger-ui#](http://localhost:7002/swagger-ui#), or
2. from the command line using the `curl` command,
3. using requests to port 7002 using networking libraries -- a python example is provided in `./async_request.py`.

All arguments available at the command line are also available for web service
requests.  We recommend trying the swagger interface to get a sense of how XML
output and parameters can be modified when using `curl`, using the web UI at
[http://localhost:7002/swagger-ui#](http://localhost:7002/swagger-ui#) after
installation. In particular, XML output files produced using different options
can be seen very quickly, but parameters for both the XML and PDF visualization
services are shown in the swagger interface.

In the requests sent by `curl`, parameters are passed directly using CGI, i.e.,
directly within the request URL after a question mark (?), with each attribute
given as a pair of parameter names and values (name=value), with 
parameters separated by ampersands (&). 

Here are some examples using`curl`, both of which work with the Javalin service started
from the command line, and with the docker container.

*XML output (words.pdf).* To obtain XML output, we call the `process-pdf`
endpoint using `curl`. In this example, we run `words.pdf` again, using default
parameters and writing the output to `xmlOutput.xml`.

```shell script
curl -X POST "http://localhost:7002/process-pdf" -H "accept: application/zip" -H "Content-Type: multipart/form-data" -F "pdf=@src/test/resources/words.pdf;type=application/pdf" > xmlOutput.xml
```

*Visualization.* Here we use our `math.pdf` example again, with CGI
parameters to apply post-processors, and to draw boxes for characters (green
boxes), text lines (yellow boxes), and lines (red boxes) in the output
file `./visualOutput.pdf`:

```shell script
curl -X POST "http://localhost:7002/draw-pdf?processors=SpliceGraphicsItemsAndCharsWithIOU&processors=IdentifyMathFromSplicedStructure&drawCharBBOX=true&drawLineBBOX=true" -H  "accept: application/pdf" -H  "Content-Type: multipart/form-data" -F "pdf=@src/test/resources/math.pdf;type=application/pdf" > visualOutput.pdf
```

## Processing the XML Text and Graphics Data Files

Whether you use Symbol Scraper as a stand-alone command line tool or as a web
service, to use the XML output data you will need a parser. Happily,
there are many mature XML libraries, and this is straight-forward. 

As an example, we have provided a simple python program `testParseXML.py` that
parses an XML output file using the BeautifulSoup library, extracts all tagged
`Char` entries, and then pretty-print them one by one to the standard output.
The program is very simple, as can be seen in the source file listing below.
The program expects the file `./xmlOutput.xml`, which you may need to create.

**testParseXML.py file listing:** 

```python
from bs4 import BeautifulSoup
   
infile = open("xmlOutput.xml","r")
contents = infile.read()
soup = BeautifulSoup(contents,'xml')
chars = soup.find_all('Char')

for character in chars:
    print( character.prettify() )

```


## Performance

The python script `async_requests.py` compares the runtimes of running the same
request multiple times from the command line, running the same requests using
the server synchonously, and using the server instance asynchronously.

**NOTE:** the asynchronous portion of the script can cause the server to fail
on some requests if the number of iterations (`iters`) is very high (50+). This
isnt so much a bug as it is the service reaching its load capacity. The number
of requests a server can handle asynchronously will depend upon the hardware
the server is run on.

To run the script you will need python3.5+.

Recommended installation (using conda):

```shell script
$ conda create -n asyncreq python=3.9
$ conda activate asyncreq
$ conda install -c anaconda requests
```

You can run the command with 10 repeated requests to produced the `math.pdf` example file:
```python
python async_requests.py --file src/test/resources/math.pdf --iters 10
```

In our case running on a linux laptop, we obtained the following output:

```shell script
time taken to run symbolscraper 10 times sync from command line: 8.245247840881348
time taken to run symbolscraper 10 times sync from docker: 0.12994837760925293  
time taken to run symbolscraper 10 times async from docker: 0.07463431358337402  
```
## Acknowledgments

**Maintainer:** Matt Langsenkamp (mattlangsenkamp@gmail.com)

**Contributors:**  Original System: Ritvik Joshi, Rewrite/Refactor (2021): Matt Langsenkamp, Earlier Refactoring (2019-2020): Alex Keller and Jessica Diehl, Extensions/Correction (2018-2019): Parag Mali, Puneeth Kukkadap, and Mahshad Mahdavi. R. Zanibbi contributed to the system strategy, design and implementation changes throughout the life of the project. 

Copyright (c) 2018 - 2021 Ritvik Joshi, Matt Langsenkamp, Alex Keller, Jessica Diehl, Parag Mali, Puneeth Kukkadapu, Mahshad Mahdavi, and Richard Zanibbi  

Developed at the Document and Pattern Recognition Laboratory
(Rochester Institute of Technology, USA)

This material is based upon work supported by the National Science Foundation (USA) under Grant Nos. IIS-1016815, IIS-1717997, and 2019897 (MMLI), and the Alfred P. Sloan Foundation under Grant No. G-2017-9827.
Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation or the Alfred P. Sloan Foundation.


